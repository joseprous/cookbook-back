module Handler.Recipes where

import Import
import Database.Persist.Sql (fromSqlKey, toSqlKey)
import Control.Lens hiding ((.=))
import Data.Aeson.Lens
import qualified Database.Esqueleto      as E
-- import           Database.Esqueleto      ((^.))
import Control.Monad.Trans.Maybe
import Control.Monad.Extra
import qualified Data.Vector as V

sendCreated :: Handler a
sendCreated = sendResponseStatus status201 ("Created" :: Text)

sendBadRequest :: Handler a
sendBadRequest = sendResponseStatus status400 ("Bad Request" :: Text)

sendDeleted :: Handler a
sendDeleted = sendResponseStatus status200 ("Deleted" :: Text)

sendNotFound :: Handler a
sendNotFound = sendResponseStatus status404 ("Not Found" :: Text)

getRecipesR :: Handler Value
getRecipesR = do
  recipes <- runDB $ selectList [] [Asc RecipeName]
  sendStatusJSON ok200 recipes

postRecipesR :: Handler ()
postRecipesR = do
  msg <- requireCheckJsonBody :: Handler Recipe
  pid <- runDB $ insert msg
  sendStatusJSON created201 (object ["id" .= fromSqlKey pid])

recipeJson :: Key Recipe -> Recipe -> Value
recipeJson k recipe = do
  object [ "id" .= k
         , "name" .= recipeName recipe
         , "text" .= recipeText recipe
         ]

getRecipeR :: Int64 -> Handler Value
getRecipeR n = do
  let recipeId = toSqlKey n :: Key Recipe
  runDB (get recipeId) >>= \case
    (Just recipe) -> sendStatusJSON ok200 $ recipeJson recipeId recipe
    Nothing -> sendNotFound

deleteRecipeR :: Int64 -> Handler ()
deleteRecipeR n = do
  runDB $ deleteCascade $ toSqlKey @Recipe n
  sendDeleted
