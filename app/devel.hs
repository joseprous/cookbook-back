{-# LANGUAGE PackageImports #-}
import "cookbook-back" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
